/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
		extend: {
			colors: {
				dc: {
					darkBlue: "#0B1C3C",
					blue: "#0487FF",
					gray: "#F6F8FB",
				},
			},
		},
	},
  plugins: [],
}