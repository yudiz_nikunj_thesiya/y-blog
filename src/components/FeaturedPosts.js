import React from 'react'
import { BsArrowLeft, BsArrowRight } from "react-icons/bs";
import BlogCard from './BlogCard';

const FeaturedPosts = () => {
  return (
    <div className="w-full py-8 px-16 lg:px-20 flex flex-col gap-6">
        <div className="w-full flex justify-between items-center">
          <h3 className="text-3xl font-bold text-dc-darkBlue">Featured Posts</h3>
          <div className="flex items-center gap-2">
            <span className="bg-dc-blue text-white p-3 rounded-full text-lg opacity-40">
              <BsArrowLeft/>
            </span>
            <span className="bg-dc-blue text-white p-3 rounded-full text-lg">
              <BsArrowRight/>
            </span>
          </div>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6">
          <BlogCard isFeatured/>
          <BlogCard isFeatured/>
          <BlogCard isFeatured/>
        </div>
      </div>
  )
}

export default FeaturedPosts