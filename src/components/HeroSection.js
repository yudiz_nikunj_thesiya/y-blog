import React from 'react'
import BlogCard from './BlogCard'

const HeroSection = () => {
  return (
    <div className="w-full py-8 px-16 lg:px-28 min-h-[600px] bg-dc-gray flex flex-col lg:flex-row items-center justify-around gap-8">
      <div className="flex flex-col gap-6 items-start">
        <h2 className="text-dc-darkBlue text-3xl md:text-4xl lg:text-6xl font-bold">Resources for <br/>makers & Creative to Learn, Sell & Grow</h2>
        <p className="text-gray-600 text-sm md:text-base lg:text-lg font-light">Attract an audience, establish, authority, build rapport <br/>and engagement with your customers.</p>
        <button className="btn-primary">+ Create a Blog</button>
      </div>
      <div className="flex">
        <div className="z-10">
          <BlogCard/>
        </div>
        <div className="scale-[90%] -ml-72">
          <BlogCard/>
        </div>
      </div>
    </div>
  )
}

export default HeroSection