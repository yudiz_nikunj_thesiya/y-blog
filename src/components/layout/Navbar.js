import React from "react";
import Logo from "../../assets/images/yudiz-logo-ltd.svg";
import { RiSearchLine } from "react-icons/ri";
import { Link } from "react-router-dom";

const Navbar = () => {
	return (
		<div className="sticky top-0 z-50">
			<div className="bg-dc-gray w-full px-4 py-3 sm:px-10 sm:py-3 items-center justify-between space-x-2 sm:space-x-0 backdrop-filter backdrop-blur-lg bg-opacity-90 grid grid-cols-2 lg:grid-cols-3">
				<span>
					<img
						src={Logo}
						objectFit="contain"
						alt="logo"
						className="cursor-pointer w-32"
					/>
				</span>

				<div className="w-full justify-center whitespace-nowrap text-base space-x-4 sm:space-x-10 hidden lg:flex">
					<span>Home</span>
					<span>Blog</span>
					<span>Check Plagarism</span>
					<span>History</span>
				</div>

				<div className="flex items-center gap-4 justify-self-end">
					<RiSearchLine />
					<Link to='/signin' className="btn-primary">Sign In</Link>
				</div>
			</div>
		</div>
	);
};

export default Navbar;
