import React from 'react'
import img from '../assets/images/img1.png'
import profileImg from '../assets/images/profile.jpg'
import { IoMdTime } from "react-icons/io";


const BlogCard = ({ isFeatured }) => {
  return (
    <div className={isFeatured ? "flex w-full bg-dc-gray rounded-2xl flex-col items-start gap-2 py-5 px-5" : "flex w-[360px] bg-white rounded-2xl flex-col items-start shadow-2xl gap-2 py-5 px-5"}>
      <img src={img} alt="img" className="w-full rounded-xl object-cover" />
      <h5 className="text-xl font-semibold text-dc-darkBlue">Fully Utilize SEO to Enhance Your Business</h5>
      <p className="text-sm text-gray-700">Search and social both provide significant digital marketing potential. But which of 
        them should be the bigger priority for your marketing team’s digital investments? </p>
      <div className="w-full flex items-end justify-between mt-4">
        <div className="flex items-center gap-2">
          <img
						src={profileImg}
						objectFit="contain"
						alt="logo"
						className="cursor-pointer w-10 h-10 rounded-full"
          />
          <div className="flex flex-col items-start">
            <h5 className="text-sm font-semibold">Haylie Aminoff</h5>
            <p className="text-xs">Sept 08, 2022</p>
          </div>
        </div>
        <div className="flex items-center text-sm gap-1">
          <IoMdTime/>
          <span className="text-xs">5 mint ago</span>
        </div>
      </div>
    </div>
  )
}

export default BlogCard