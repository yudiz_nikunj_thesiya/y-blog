import RegisterImg from "../assets/images/login.jpeg";
import Logo from "../assets/images/yudiz-logo-ltd.svg";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";

function SignIn() {
	return (
		<div className="w-full flex relative overflow-hidden">
			<motion.div
				className="flex flex-col items-start min-h-screen w-full lg:w-1/2 bg-white rounded-2xl p-10 sm:px-12 sm:py-12 border border-transparent text-sm sm:text-base"
				initial={{ x: "100vw" }}
				animate={{ x: 0 }}
			>
				<div className="w-full flex items-center justify-between mb-8"> 
					<h2 className="text-dc-blue text-2xl md:text-3xl lg:text-4xl font-bold">
						Sign In
					</h2>
					<img
						src={Logo}
						objectFit="contain"
						alt="logo"
						className="cursor-pointer w-32"
					/>
				</div>

				<form className="w-full flex flex-col space-y-8 mb-7">
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Email Address
						</label>
						<input
							type="text"
							name="name"
							placeholder="Email *"
							className="input"
						/>
					</div>
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Password
						</label>
						<input
							type="text"
							name="name"
							placeholder="Password *"
							className="input"
						/>
					</div>

					<button type="submit" className="btn-primary">
						Sign In
					</button>
				</form>

				<div className="flex w-full mb-12 md:mb-0 items-center justify-center md:text-lg space-x-2">
					<p className="text-gray-600">Create a new account.</p>
					<Link to="/signup"
						className="text-dc-purple cursor-pointer hover:underline"
					>
						Register
					</Link>
				</div>
			</motion.div>
			<motion.div
				className="w-1/2 p-12 hidden lg:flex items-center bg-dc-gray"
				initial={{ x: "-100vw" }}
				animate={{ x: 0 }}
			>
				<img
					src={RegisterImg}
					alt="img"
					className="object-cover w-full h-full rounded-3xl"
				/>
			</motion.div>
		</div>
	);
}

export default SignIn;
