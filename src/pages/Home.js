import React from 'react'
import HeroSection from '../components/HeroSection'
import Layout from '../components/layout/Layout'
import FeaturedPosts from '../components/FeaturedPosts';

const Home = () => {
  return (
    <Layout>
      <HeroSection />
      <FeaturedPosts/>
    </Layout>
  )
}

export default Home