import RegisterImg from "../assets/images/register.jpeg";
import { motion } from "framer-motion";
import Logo from "../assets/images/yudiz-logo-ltd.svg";
import { Link } from "react-router-dom";

function SignUp() {
	return (
		<div className="w-full flex relative overflow-hidden">
			<motion.div
				className="w-1/2 p-12 hidden lg:flex items-center justify-center bg-dc-gray min-h-screen"
				initial={{ x: "100vw" }}
				animate={{ x: 0 }}
			>
        <img
          alt="img"
					src={RegisterImg}
					className="object-cover w-full h-full rounded-3xl"
				/>
			</motion.div>
			<motion.div
				className="flex flex-col items-start w-full lg:w-1/2 bg-white rounded-2xl p-10 sm:px-12 sm:py-12 border border-transparent text-sm sm:text-base"
				initial={{ x: "-100vw" }}
				animate={{ x: 0 }}
			>
				<div className="w-full flex items-center justify-between mb-8">

				<h2 className="text-dc-blue text-2xl md:text-3xl lg:text-4xl font-bold">
					Sign Up
				</h2>
				<img
						src={Logo}
						objectFit="contain"
						alt="logo"
						className="cursor-pointer w-32"
					/>
				</div>

				<form className="w-full flex flex-col space-y-8 mb-7">
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Your Name
						</label>
						<input
							type="text"
							name="name"
							placeholder="Your Name *"
							className="input"
						/>
					</div>
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Email Address
						</label>
						<input
							type="text"
							name="name"
							placeholder="Email *"
							className="input"
						/>
					</div>
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Password
						</label>
						<input
							type="text"
							name="name"
							placeholder="Password *"
							className="input"
						/>
					</div>
					<div className="flex flex-col items-start text-gray-500">
						<label for="name" className="text-base sm:text-lg sr-only">
							Confirm Password
						</label>
						<input
							type="text"
							name="name"
							placeholder="Confirm Password *"
							className="input"
						/>
					</div>

					<button type="submit" className="btn-primary">
						Submit
					</button>
				</form>

				<div className="flex w-full mb-12 md:mb-0 items-center justify-center md:text-lg space-x-2">
					<p className="text-gray-600">Already Member?</p>
					<Link to="/signin"
						className="text-dc-purple cursor-pointer"
					>
						Sign In
					</Link>
				</div>
			</motion.div>
		</div>
	);
}

export default SignUp;
